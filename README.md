# VulkanTutorial
## 基础环境搭建
### 下载
- [Vulkan SDK](https://vulkan.lunarg.com/)
- [GLFW](https://www.glfw.org/download.html)
- [GLM](https://github.com/g-truc/glm/releases/)

![image](Images/library_directory.png)

### VS配置

![image](Images/vs_all_configs.png)

![image](Images/vs_cpp_general.png)

![image](Images/vs_include_dirs.png)

![image](Images/vs_link_settings.png)

![image](Images/vs_link_dirs.png)

![image](Images/vs_link_input.png)

![image](Images/vs_dependencies.png)

![image](Images/vs_cpp17.png)
